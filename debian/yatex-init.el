;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; /etc/emacs/site-start.d/50yatex-init.el
;; $Id: yatex-init.el,v 1.2 2003/06/20 00:26:35 dombly Exp $
;;
;; Copyright (C) 2003  Debian Project
;; (Maintained by INOUE Hiroyuki <dombly@kc4.so-net.ne.jp>)
;;
;;
;; NOTE: Lines with tab-stop indents are indispensable for sake of
;;       the system's sanity.  Editing them is discouraged.
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;    (setq auto-mode-alist (append '(

;;;;;; [auto-mode-alist]
;;
;; for YaTeX
;;
;("\\.tex$" . yatex-mode)
;("\\.ltx$" . yatex-mode)
;("\\.cls$" . yatex-mode)
;("\\.sty$" . yatex-mode)
;("\\.clo$" . yatex-mode)
;("\\.bbl$" . yatex-mode)

;;
;; for YaHTML
;;
;("\\.s?html?$" . yahtml-mode) ; disabled by default


;    ) auto-mode-alist))
;;;;;; [YaTeX-japan]
;;
;; By default, YaTeX/YaHTML show Japanese messages if possible.
;; You can disable this feature here.
;;
;(setq YaTeX-japan nil)



;;;;;; [External programs]
;;
;; You can configure site-wide defaults of external programs here.
;; (Those lines which are disabled show the upstream default.)
;;
;;
;; YaTeX typesetting command
;;
;(setq tex-command
;      (cond
;       (YaTeX-use-LaTeX2e "platex")
;       (YaTeX-japan "jlatex")
;       (t "latex")))

;;
;; YaTeX BibTeX command
;;
;(setq bibtex-command (if YaTeX-japan "jbibtex" "bibtex"))

;;
;; YaTeX MakeIndex command
;;
;(setq makeindex-command "makeindex")

;;
;; YaTeX previewer command
;;
;(setq dvi2-command "xdvi -geo +0+0 -s 4")

;;
;; YaTeX command for printing
;;  - Command line string to print out current file.
;;    Format string %s will be replaced by the filename.  Do not forget to
;;    specify the `from usage' and `to usage' with their option by format
;;    string %f and %t.
;;    See also documentation of dviprint-from-format and dviprint-to-format.
;;
;(setq dviprint-command-format "dvi2ps %f %t %s | lpr")


;;
;; YaHTML lint command
;;
;(setq yahtml-lint-program (if YaTeX-japan "jweblint" "weblint"))

;;
;; YaHTML browser
;;
;(setq yahtml-www-browser "iceweasel")

;;
;; YaHTML image viewer
;;
;(setq yahtml-image-viewer "xv") ; upstream default - unavailable
(setq yahtml-image-viewer "see") ; free alternative for Debian

;; YaTeX kanji-code
;;
(setq YaTeX-kanji-code nil)

;; YaTeX prefix-key
(setq YaTeX-inhibit-prefix-letter t)

;;
;; End of file `yatex-init.el'
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
